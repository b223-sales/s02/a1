<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>A02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
	<h1>Divisible of Five</h1>

	<?php printDivisibleOfFive(); ?>

	<h1>Array Manipulation</h1>

	<?php foreach($studentsArray as $students) { ?>
	 	<pre><?php var_dump($students); ?></pre>
	<?php } ?>

	<pre><?php echo count($studentsArray); ?></pre>

	<?php array_push($studentsArray, 'Jane Smith'); ?>
	 <pre><?php var_dump($studentsArray); ?></pre>

	 <pre><?php echo count($studentsArray); ?></pre>

	 <?php array_shift($studentsArray); ?>
	 <pre><?php var_dump($studentsArray); ?></pre>

	 <pre><?php echo count($studentsArray); ?></pre>




</body>
</html>